#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"

if hash npm 2>/dev/null; then
    # https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally
    npm config set prefix '~/.local/share/npm-global'
    while read in; do 
        _info "installing $in"
        npm -g i "$in"; 
    done < "$dirname/packages/node-packages.txt"
fi
