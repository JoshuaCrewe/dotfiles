# Vim

I keep this config around just in case I want to bail out of the neovim life and go back to Vimscript. I haven't yet! I am probably more likely to start again from scratch. Also useful to test Seoul256 colour scheme from the proper repo.
