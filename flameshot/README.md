# Flameshot

[https://flameshot.org/](https://flameshot.org/)

This is one of those apps which work on both X and Wayland (but I have my doubts about that). A screenshot tool should allow you to markup the image with arrows and squares and whatnot. Flameshot has some nice keyboard shortcuts which really help with this workflow.
