# Rofi

[https://github.com/davatorium/rofi](https://github.com/davatorium/rofi)

I don't love this application but it seems to be the best of the bunch. Sometimes I wonder about going back to dmenu for all of the features I use of Rofi. It exists to fill the alt-tab itch (which I don't really use that much) and from the days when Alfred was being used.

One of the things Alfred was good at was being one interface for everything. The opening view was a minimal input bar and you could put pretty much anything in to it and it would get you what you wanted. Launch an application. Yep. Do some maths? No problem. Rofi can do these things but it isn't as seamless. There is always an open menu which I don't love.

Apparently it works on Wayland as well.
