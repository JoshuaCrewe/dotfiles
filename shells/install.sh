#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"

src="$dirname/config/zsh/zshrc"
dst="$HOME/.zshrc"
_link "$src" "$dst"

src="$dirname/config/zsh/zshrc"
dst="$HOME/.config/zsh/.zshrc"
_link "$src" "$dst"

src="$dirname/config/bash/bashrc"
dst="$HOME/.bashrc"
_link "$src" "$dst"

# Kick it into action ?
# This should take care of plugins too :)
# source $(HOME)/.zshrc
