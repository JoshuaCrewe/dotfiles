export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache

export ZDOTDIR=$HOME/.config/zsh

export CARGO_HOME="$XDG_DATA_HOME"/cargo

export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv

export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker

export GNUPGHOME="$XDG_DATA_HOME"/gnupg

export MBSYNCRC=${XDG_CONFIG_HOME:-~/.config}/mbsync/config

export LESSHISTFILE="$XDG_STATE_HOME"/less/history

export FZF_MARKS_FILE=$XDG_CONFIG_HOME/fzf-marks/marks

export W3M_DIR=$XDG_CONFIG_HOME/w3m
