export FZF_DEFAULT_OPTS="--color 'bg+:#121212,border:#121212'"

# https://github.com/webpack/webpack/issues/14532#issuecomment-947012063
# export NODE_OPTIONS=--openssl-legacy-provider

source $DOTFILES/shells/config/shared/exports/clean-home.sh

if [ -f $DOTFILES/shells/config/shared/exports/secrets.sh ]; then
    source $DOTFILES/shells/config/shared/exports/secrets.sh
fi

export PATH=/opt/google-cloud-cli/bin:$PATH

# https://wiki.archlinux.org/title/SSH_keys#Start_ssh-agent_with_systemd_user
export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ssh-agent.socket

# Tell capacitor where Android Studio is
export CAPACITOR_ANDROID_STUDIO_PATH=/opt/android-studio/bin/studio
