if hash nvim 2>/dev/null; then
    alias vimo='nvim --clean -u ~/.config/vim/init.vim'

    if [ -v NVIM_LISTEN_ADDRESS ];then
        type -p :e > /dev/null && alias vim=':e' || alias vim='nvim -u ~/.config/nvim/init.lua'
    else
        alias vim='nvim -u ~/.config/nvim/init.lua'
    fi

    if [ -v NVIM_LISTEN_ADDRESS ];then
        type -p exa > /dev/null && exa --tree --level=1 --group-directories-first $PWD || bash -c "pwd && ls -1"
    fi
fi

alias vi='vim'
alias vimrc='vim -c e $DOTFILES/vim/config/vimrc'
alias zshrc='vim -c e $DOTFILES/shells/config/zsh/zshrc'
alias tmuxconf='vim -c e $DOTFILES/tmux/config/tmux.conf'

# alias n='notes'
# alias n='nix run "nixpkgs#nb" --'
alias n='nb'
