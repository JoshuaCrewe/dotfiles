export _plugin_path="$HOME/.local/share/zsh/plugins"
if [[ ! -d $_plugin_path ]]; then
    mkdir -p $_plugin_path
fi

if [[ ! -f $_plugin_path/zsh-autosuggestions/zsh-autosuggestions.zsh ]]; then
    command git clone https://github.com/zsh-users/zsh-autosuggestions $_plugin_path/zsh-autosuggestions
fi
source $_plugin_path/zsh-autosuggestions/zsh-autosuggestions.zsh
bindkey '^]' autosuggest-accept

if [[ ! -f $_plugin_path/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh ]]; then
    command git clone https://github.com/zdharma-continuum/fast-syntax-highlighting $_plugin_path/fast-syntax-highlighting/
fi
source $_plugin_path/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh

if [[ ! -f $_plugin_path/fzf-marks/fzf-marks.plugin.zsh ]]; then
    command git clone https://github.com/urbainvaes/fzf-marks.git $_plugin_path/fzf-marks
fi
source $_plugin_path/fzf-marks/fzf-marks.plugin.zsh

if [[ ! -d $_plugin_path/pure/ ]]; then
    command git clone https://github.com/sindresorhus/pure.git $_plugin_path/pure
fi
fpath+=$_plugin_path/pure
autoload -U promptinit; promptinit
prompt pure

autoload -U zcalc
function __calc_plugin {
    zcalc -f -e "$*"
}
aliases[calc]='noglob __calc_plugin'
aliases[=]='noglob __calc_plugin'

function update-zsh-plugins() {
    for plug in $HOME/.local/share/zsh/plugins/*; do
        echo "update $(basename $plug)"
        bash -c "cd $plug && git pull"
    done
}
