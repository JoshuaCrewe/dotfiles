background() {
    for ((i=2;i<=$#;i++)); do
        ${@[1]} ${@[$i]} &> /dev/null &
    done
}

alias -s {pdf,PDF}='background zathura'
alias -s {jpg,JPG,png,PNG}='background sxiv'
alias -s {html,HTML}="background $BROWSER" # This doesn't work
alias -s {zip,ZIP,war,WAR}="unzip -l"
alias -s {txt,TXT}="vim"
