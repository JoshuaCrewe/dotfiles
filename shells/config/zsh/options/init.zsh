source $DOTFILES/shells/config/zsh/options/checks.zsh
source $DOTFILES/shells/config/zsh/options/completions.zsh
source $DOTFILES/shells/config/zsh/options/exports.zsh
source $DOTFILES/shells/config/zsh/options/setopt.zsh
source $DOTFILES/shells/config/zsh/options/history.zsh
source $DOTFILES/shells/config/zsh/options/bindkey.zsh
source $DOTFILES/shells/config/zsh/options/nnn.zsh
