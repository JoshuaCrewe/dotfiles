# Tridactyl

On the surface this seems like a really solid option. All the popularity of a main stream browser like Firefox but with the Qutebrowser naviagtion.

Unfortunately Firefox has crippled what extensions can do, so you end up in situations where the addon doesn't work. Also, websites suck, so you can't always navigate around using the keyboard. This isn't really a problem with Tridactyl as it is the same on Qutebrowser too.

## Default Key Mappings

Firefox has its own keymappings which can get in the way. You can't currently unbind these in Tridactyl. There is a plugin which can though!

[https://github.com/crittermike/shortkeys](https://github.com/crittermike/shortkeys)
