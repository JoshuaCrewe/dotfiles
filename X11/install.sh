#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"


if [[ $is_linux == 1 ]]; then
    src="$dirname/config/Xresources"
    dst="$HOME/.config/X11/Xresources"
    _link "$src" "$dst"

    src="$dirname/config/themes"
    dst="$HOME/.config/X11/themes"
    _link "$src" "$dst"

    src="$dirname/config/xinitrc"
    dst="$HOME/.xinitrc"
    _link "$src" "$dst"

    src="$dirname/config/xorg.conf.d/20-intel.conf"
    dst="/etc/X11/xorg.conf.d/20-intel.conf"
    _sudo_link "$src" "$dst"

    src="$dirname/config/xorg.conf.d/30-touchpad.conf"
    dst="/etc/X11/xorg.conf.d/30-touchpad.conf"
    _sudo_link "$src" "$dst"

    src="$dirname/config/user-dirs.dirs"
    dst="$HOME/.config/user-dirs.dirs"
    _link "$src" "$dst"

    src="$dirname/config/user-dirs.locale"
    dst="$HOME/.config/user-dirs.locale"
    _link "$src" "$dst"

    src="$dirname/config/mimeapps.list"
    dst="$HOME/.config/mimeapps.list"
    _link "$src" "$dst"

fi
