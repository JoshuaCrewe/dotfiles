# X11

People say that you need to get off Xorg and get on the Wayland train. They are probably right but you know what works now? Without needing to configure a new thing? Yeah, it is X11.

There is a path to success which leads to Wayland. One day there will be enough momentum to switch but the biggest issues with that right now are 

- Deciding on alternative applications for pretty much everything
- Switching Window Manager

The former is a bit of a warren as each application choice will need to be reconfigured which is a time issue. The latter is a big one along with choosing a bar (or not having a bar) and as this becomes the window on to the machine, changing it is a big deal.

The config in here isn't huge mind, things like stopping screen tearing (why is this an extra config and not on by default?) plus some init config and a theme. Easy.
