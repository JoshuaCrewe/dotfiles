local sig = [[ ]]

function Para(elem)
  if elem.content[1].text == "{{signature}}" then
    return pandoc.read(sig, 'html').blocks
  end
end
