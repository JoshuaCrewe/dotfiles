#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"

src="$dirname/config/user"
dst="$HOME/.config/mutt/user"
_link "$src" "$dst"

src="$dirname/config/templates"
dst="$HOME/.local/share/pandoc/templates"
_link "$src" "$dst"

src="$dirname/config/urlview"
dst="$HOME/.urlview"
_link "$src" "$dst"

if hash mw 2>/dev/null; then
  src="$dirname/config/systemd/mutt-wizard-mailsync.service"
  dst="$HOME/.config/systemd/user/mutt-wizard-mailsync.service"
  chmod 644 "$src"
  _link "$src" "$dst"

  src="$dirname/config/systemd/mutt-wizard-mailsync.timer"
  dst="$HOME/.config/systemd/user/mutt-wizard-mailsync.timer"
  chmod 644 "$src"
  _link "$src" "$dst"

  _user_enable 'mutt-wizard-mailsync.timer'
fi
