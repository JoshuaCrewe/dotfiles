#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

is_linux=0
is_mac=0

if [[ $(uname) = 'Linux' ]]; then
    is_linux=1
fi

if [[ $(uname) = 'Darwin' ]]; then
    is_mac=1
fi

_info() {
	# shellcheck disable=SC2059
	printf "\r  [ \033[00;34m..\033[0m ] $1\n"
}

_user() {
	# shellcheck disable=SC2059
	printf "\r  [ \033[0;33m??\033[0m ] $1\n"
}

_warn() {
	# shellcheck disable=SC2059
	printf "\r  [ \033[0;33mWARN\033[0m ] $1\n"
}

_success() {
	# shellcheck disable=SC2059
	printf "\r\033[2K  [ \033[00;32mDONE\033[0m ] $1\n"
}

_backup() {
	# shellcheck disable=SC2059
	printf "\r\033[2K  [ \033[00;34mBACKUP\033[0m ] $1\n"
}

_enable() {
	# shellcheck disable=SC2059
	printf "\r\033[2K  [ \033[00;32mENABLED\033[0m ] $1\n"
}

_start() {
	# shellcheck disable=SC2059
	printf "\r\033[2K  [ \033[00;32mSTARTED\033[0m ] $1\n"
}

_fail() {
	# shellcheck disable=SC2059
	printf "\r\033[2K  [\033[0;31mFAIL\033[0m] $1\n"
	echo ''
	exit
}

_link () {
    if [ ! -d "$(dirname "$2")" ]; then
        mkdir -p "$(dirname "$2")"
    fi

	if [ -f "$2" ] || [ -d "$2" ]; then
        _warn "$2 already exists"
        return
	fi
	ln -sf "$1" "$2"
	_success "linked $1 to $2"
}

_sudo_link () {
	if [ -f "$2" ] || [ -d "$2" ]; then
        _warn "$2 already exists"
        return
	fi
	sudo ln -sf "$1" "$2"
	_success "linked $1 to $2"
}

_sudo_enable () {
    sudo systemctl enable "$1" > /dev/null
    _enable "$1"
}

_sudo_start () {
    sudo systemctl start "$1" > /dev/null
    _start "$1"
}

_user_enable () {
    systemctl --user enable --now "$1" > /dev/null
    _enable "$1"
}

_install () {
    if ! hash $1 2>/dev/null; then
        if hash brew 2>/dev/null; then
            brew install "$1"
            _success "$1 installed"
        elif hash pacman 2>/dev/null; then
            sudo pacman --noconfirm -S "$1"
            _success "$1 installed"
        fi
    else
        _success "$1 is already installed"
    fi
}

_aur () {
    if ! hash $1 2>/dev/null; then
        if hash paru 2>/dev/null; then
            paru --noconfirm -S "$1"
            _success "$1 installed"
        fi
    else
        _success "$1 is already installed"
    fi
}
