#!/usr/bin/env bash

noteFilename="$HOME/.local/share/notes/🦄/meetings/$(date +%Y-%m-%d::%H:%M).md"
# if [ -f $noteFilename ]; then
#   noteFilename="$HOME/.local/share/notes/🦄/meetings/$(date +%Y-%m-%d::%H:%M).md"
# fi


if [ ! -f $noteFilename ]; then
  # echo "# Meeting Notes for $(date '+%a %d %B %4Y')" > $noteFilename
  nvim \
    -c "norm GO# Meeting Notes for $(date '+%a %d %B %4Y')" \
    -c "norm Go## Notes" \
    -c "norm G2o" \
    -c "norm Go## Questions" \
    -c "norm G2o" \
    -c "norm Go## Actions" \
    -c "norm G2o" \
    -c "norm gg4jO" \
    -c "startinsert" $noteFilename
else
    nvim $noteFilename
fi
