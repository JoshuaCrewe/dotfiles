#!/bin/sh

noteFilename="$HOME/.local/share/notes/🦄/inbox/note-$(date +%d-%m-%Y).md"

if [ ! -f $noteFilename ]; then
  echo "# $(date "+%d %B %y") : " > $noteFilename
fi

nvim -c "norm Go" \
  -c "norm Go## $(date +%H:%M)" \
  -c "norm G2o" \
  -c "norm zz" \
  -c "startinsert" $noteFilename

