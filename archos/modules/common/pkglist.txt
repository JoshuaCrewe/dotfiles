# Core Packages
# =============
base
base-devel
cronie
fwupd
interception-dual-function-keys
iwd
man-db
man-pages
neovim
openssh
pacman
pacman-contrib
pipewire-jack
pipewire-pulse
kitty
linux
linux-firmware
pulsemixer
slock
sshfs
tmux
vi
wget
systemd-resolvconf
zip
unzip
fzf
imagemagick
zsh
git

pellets [aur]
paru-bin [aur]
paru-bin-debug [aur]
