````
#
# https://github.com/dpatti/pellets
# ```
# sudo pacman -Q --quiet --explicit --native >! pkglist.txt && pacman -Q --quiet --explicit --foreign | xargs printf "%s [aur]\n" >> pkglist.txt
# ```
#
````

## Flatpaks

`flatpak list --columns=application --app > flatpaks.txt`
`xargs flatpak install -y < flatpaks.txt`
