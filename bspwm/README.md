# BSPWM

[https://github.com/baskerville/bspwm](https://github.com/baskerville/bspwm)

This setup sort of requires [sxhkd](https://github.com/baskerville/sxhkd) to work as there isn't a connection between keyboard inputs and the window manager by default.

There are a few niche things setup on this WM that are specific to things I am trying out : 

- Notes from a key combination to a scratch window
- Multiple monitors with different polybar configs

I love that it is all done in Bash. I don't love that it can be a bit hacky sometimes. Like what is that external-programs.sh script doing there? I don't remember.
