acmd({ "FileType" },
    {
        pattern = "php",
        -- command = "setlocal commentstring=//%s | set autoindent | set indentexpr=false"
        command = "setlocal commentstring=//%s | set autoindent | set indentexpr=true"
    })

acmd({ "BufEnter" },
    {
        pattern = "*.scss",
        command = "set shiftwidth=4"
    })

acmd({ "FileType" },
    {
        pattern = "*.twig",
        command = "setlocal commentstring={#%s#}"
    })
acmd({ "BufEnter" },
    {
        pattern = "*.twig",
        command = "set ft=html.twig"
    })


acmd({ "FileType" },
    {
        pattern = "markdown",
        command = "setl makeprg=pen\\ %\\ &&"
    })


-- @TODO: move this chunk to its own settings file for replacing netrw
-- Maybe even make it a plugin?

-- Terminal opens, we can get typing
acmd({ "TermOpen" }, { command = "startinsert" })
-- don't need line numbers in a terminal
acmd({ "TermOpen" }, { command = "setlocal nonumber norelativenumber" })
-- Close the buffer when the terminal process ends
acmd({ "TermClose" }, { command = "if !v:event.status | exe 'bdelete! '..expand('<abuf>') | endif" })

-- Pass on the remote listening address to the shell
vim.fn['setenv']('NVIM_LISTEN_ADDRESS', vim.v.servername)
vim.fn['setenv']('NVIM_PROJECT_ROOT', vim.fn.getcwd())
