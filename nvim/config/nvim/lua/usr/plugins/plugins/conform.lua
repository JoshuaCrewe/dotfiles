return {
  {
    'stevearc/conform.nvim',
    config = function()
      require("conform").setup({
        format_on_save = {
          -- These options will be passed to conform.format()
          timeout_ms = 500,
          lsp_format = "never",
        },
        formatters_by_ft = {
          javascript = { "prettier", stop_after_first = true, lsp_format = 'fallback' },
          vue = { "prettier", stop_after_first = true, lsp_format = 'fallback' },
          typescript = { "prettier", stop_after_first = true, lsp_format = 'fallback' },
        },
        prepend_args = function(self, ctx)
          local util = require("conform.util")
          -- you may want to include all the possible config file names
          -- see https://github.com/stevearc/conform.nvim/blob/46c107ad0e7d83b5a7091112ec2994c847577d32/lua/conform/formatters/prettier.lua#L64-L77
          local config_file = vim.fs.find({ ".prettierrc", "prettierrc.json" },
            { upward = true, path = ctx.dirname })[1]

          if config_file then
            return {} -- don't need to return anything because it will pick up the file from the cwd
          else
            -- return { "--config=" .. vim.fn.expand("~/.config/nvim/prettierrc.json") }
          end
        end
      })
    end
  }
}
