return {
  {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    dependencies = {
      'JoosepAlviste/nvim-ts-context-commentstring',
      'nvim-treesitter/playground',
    },
    config = function()
      require 'nvim-treesitter.configs'.setup {
        -- A list of parser names, or "all"
        ensure_installed = { "vimdoc", "html", "javascript", "typescript", "lua", "rust", "bash", "vue" },
        -- ensure_installed = 'all',

        modules = {},

        ignore_install = {},

        indent = { enable = true },

        -- Install parsers synchronously (only applied to `ensure_installed`)
        sync_install = true,

        -- Automatically install missing parsers when entering buffer
        -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
        auto_install = false,

        highlight = {
          -- `false` will disable the whole extension
          enable = true,

          disable = {
            -- "scss", -- There is zero variety in colours
            "html",             -- This is just a bit too broken to be useful
            -- "twig"  -- This is just a bit too broken to be useful
          },

          -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
          -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
          -- Using this option may slow down your editor, and you may see some duplicate highlights.
          -- Instead of true it can also be a list of languages
          additional_vim_regex_highlighting = {
            -- "org", -- The README suggests this
            -- "php", -- There is a bug where closing tags are not highlighted
            -- "twig" -- There is a bug where closing tags are not highlighted
            -- "gitcommit"
          },

          context_commentstring = {
            enable = true
          }
        },
      }
    end
  },
}
