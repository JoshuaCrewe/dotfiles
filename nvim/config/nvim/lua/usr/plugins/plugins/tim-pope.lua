return {
    { 'tpope/vim-repeat'}, -- Allow dot command for more things e.g. vim-surround
    { 'tpope/vim-surround' }, -- Surrounding text motion
    { 'tpope/vim-unimpaired' }, -- Some extra shortcuts for useful tasks
    { 'tpope/vim-ragtag' }, -- Some extra mappings
    { 'tpope/vim-fugitive' }, -- Turns out I really like git blame
}
