return {
    {
        -- dir = "~/Data/code/seoul256",
        "https://codeberg.org/JoshuaCrewe/seoul256.nvim",
        lazy = false,    -- make sure we load this during startup if it is your main colorscheme
        priority = 1000, -- make sure to load this before all the other start plugins
        dependencies = {
            "rktjmp/lush.nvim"
        },
        config = function()
            opt.termguicolors = true
            opt.background = 'dark'
            g.seoul256_background = 233

            local colorscheme = "seoul256"
            local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
            if not status_ok then
                vim.notify("colorscheme " .. colorscheme .. " not found.")
                return
            end

            -- plugin/telescope.lua:11
            cmd [[highlight TelescopeSelection ctermbg=234]]
            cmd [[highlight TelescopeMatching  ctermbg=237]]
        end
    }
}
