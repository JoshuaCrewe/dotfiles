return {
    "https://codeberg.org/JoshuaCrewe/telescope-notmuch.nvim.git",
    dependencies = {
        'nvim-telescope/telescope.nvim',
    },
    config = function()
        require('telescope').load_extension("notmuch")
    end,
    ft = { 'mail' }
}
