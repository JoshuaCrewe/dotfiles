return {
    {
        'nvim-telescope/telescope.nvim',
        dependencies = {
            "nvim-lua/popup.nvim",
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope-live-grep-args.nvim",
        },
        config = function()
            local status_ok, telescope = pcall(require, "telescope")
            if not status_ok then
                return
            end

            local lga_actions = require("telescope-live-grep-args.actions")

            telescope.setup {
                extensions = {
                    live_grep_args = {
                        -- auto_quoting = true, -- enable/disable auto-quoting
                        mappings = { -- extend mappings
                            i = {
                                ["<C-k>"] = lga_actions.quote_prompt(),
                                ["<C-i>"] = lga_actions.quote_prompt({ postfix = " --iglob " }),
                            },
                        },
                        vimgrep_arguments = { "rg", "--no-heading", "--with-filename", "--line-number", "--column", "--smart-case", "--glob=!dist/", "--glob=!.git/" }
                    }
                },
                defaults = {
                    file_previewer   = require('telescope.previewers').vim_buffer_cat.new,
                    grep_previewer   = require('telescope.previewers').vim_buffer_vimgrep.new,
                    qflist_previewer = require('telescope.previewers').vim_buffer_qflist.new,
                    mappings         = {
                        i = {
                            -- ["<C-j>"] = actions.move_selection_next,
                            -- ["<C-k>"] = actions.move_selection_previous,
                        },
                    },
                },
                pickers = {
                    find_files = {
                        -- `hidden = true` will still show the inside of `.git/` as it's not `.gitignore`d.
                        find_command = { "rg", "--files", "--hidden", "--glob", "!**/dist/*", "--glob=!.git/" },
                    },
                },
            }
            require("telescope").load_extension("live_grep_args")

            -- Globally lookging for files
            map("n", "<CR>", ":Telescope find_files<CR>")
            -- Searching globally for a pattern
            map("n", "<leader><CR>", ":Telescope live_grep_args<CR>")
            -- Quick select another buffer (leader right shift)
            map("n", "<leader>)", ":Telescope buffers<CR>")
            map("n", "<leader>q", ":Telescope quickfix<CR>")

            map("n", "z=", ":Telescope spell_suggest<CR>")

            -- Quickly pop an email address in
            map("i", "<c-x><c-p>", "<cmd>Telescope notmuch theme=cursor<CR>")
        end
    }
}
