return {
    {
        'L3MON4D3/LuaSnip',
        dependencies = {
            'honza/vim-snippets'
        },
        config = function()
            require("luasnip.loaders.from_snipmate").lazy_load()

            -- vim.api.nvim_set_keymap('i', '<Tab>', 'luasnip#expand_or_jumpable() ? "<Plug>luasnip-expand-or-jump" : "<Tab>"', {expr = true, silent = true})
            -- vim.api.nvim_set_keymap('i', '<S-Tab>', '<cmd>lua require"luasnip".jump(-1)<Cr>', {})

            -- vim.api.nvim_set_keymap('s', '<Tab>', '<cmd>lua require("luasnip").jump(1)<Cr>', {})
            -- vim.api.nvim_set_keymap('s', '<S-Tab>', '<cmd>lua require("luasnip").jump(-1)<Cr>', {})

            -- vim.api.nvim_set_keymap('i', '<C-E>', 'luasnip#choice_active() ? "<Plug>luasnip-next-choice" : "<C-E>"', {expr = true, silent = true})
            -- vim.api.nvim_set_keymap('s', '<C-E>', 'luasnip#choice_active() ? "<Plug>luasnip-next-choice" : "<C-E>"', {expr = true, silent = true})

            require 'luasnip'.filetype_extend("php", { "html" })

            require('luasnip').config.set_config({
                history = false,
                updateevents = "TextChanged, TextChangedI",
                region_check_events = 'CursorMoved',
            })
        end
    }
}
