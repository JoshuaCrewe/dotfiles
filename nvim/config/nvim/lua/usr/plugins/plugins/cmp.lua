return {
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'saadparwaiz1/cmp_luasnip',
      'hrsh7th/cmp-nvim-lsp',
    },

    config = function()
      local cmp = require('cmp')
      local cmp_action = require('lsp-zero').cmp_action()

      local luasnip = require("luasnip")

      cmp.setup({
        view = {
          entries = "custom" -- can be "custom", "wildmenu" or "native"
        },
        window = {
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },
        sources = {
          { name = 'nvim_lsp' },
          { name = 'luasnip' },
          { name = "neorg" },
          { name = "tailwindcss" },
          { name = "buffer" },
        },
        experimental = {
          ghost_text = { hl_group = 'NonText' }
        },
        -- completion = {  }
        completion = {
          -- autocomplete = true,
          autocomplete = { require('cmp.types').cmp.TriggerEvent.TextChanged },
          completeopt = 'menu,menuone,noselect'
        },
        -- performance = {
        --     debounce = 2000 -- this isn't working as expected
        -- },
        mapping = cmp.mapping.preset.insert({

          -- https://github.com/hrsh7th/nvim-cmp/wiki/Example-mappings#safely-select-entries-with-cr
          ["<CR>"] = cmp.mapping({
            i = function(fallback)
              if cmp.visible() and cmp.get_active_entry() then
                cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
              elseif luasnip.expandable() then
                luasnip.expandable()
              else
                fallback()
              end
            end,
            s = cmp.mapping.confirm({ select = true }),
            c = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true }),
          }),

          -- https://github.com/hrsh7th/nvim-cmp/wiki/Example-mappings#luasnip
          -- ['<CR>'] = cmp.mapping(function(fallback)
          --     if cmp.visible() then
          --         if luasnip.expandable() then
          --             luasnip.expand()
          --         else
          --             cmp.confirm({
          --                 select = true,
          --             })
          --         end
          --     else
          --         fallback()
          --     end
          -- end),

          ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif luasnip.locally_jumpable(1) then
              luasnip.jump(1)
            else
              fallback()
            end
          end, { "i", "s" }),

          ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif luasnip.locally_jumpable(-1) then
              luasnip.jump(-1)
            else
              fallback()
            end
          end, { "i", "s" }),



          -- Navigate between completion items
          ['<C-p>'] = cmp.mapping.select_prev_item({ behavior = 'select' }),
          ['<C-n>'] = cmp.mapping.select_next_item({ behavior = 'select' }),

          -- `Enter` key to confirm completion
          -- ['<CR>'] = cmp.mapping.confirm({select = false}),

          -- Ctrl+Space to trigger completion menu
          ['<C-Space>'] = cmp.mapping.complete(),

          -- Navigate between snippet placeholder
          ['<C-f>'] = cmp_action.vim_snippet_jump_forward(),
          ['<C-b>'] = cmp_action.vim_snippet_jump_backward(),

          -- Scroll up and down in the completion documentation
          ['<C-u>'] = cmp.mapping.scroll_docs(-4),
          ['<C-d>'] = cmp.mapping.scroll_docs(4),

        }),

        snippet = {
          expand = function(args)
            require 'luasnip'.lsp_expand(args.body)
          end,
        },

        -- https://github.com/hrsh7th/nvim-cmp/wiki/Advanced-techniques#disabling-completion-in-certain-contexts-such-as-comments
        enabled = function()
          -- disable completion in comments
          local context = require 'cmp.config.context'
          -- keep command mode completion enabled when cursor is in a comment
          if vim.api.nvim_get_mode().mode == 'c' then
            return true
          else
            return not context.in_treesitter_capture("comment")
                and not context.in_syntax_group("Comment")
          end
        end
      })

      -- This it to stop the menu popping up in context where there isn't much else to know
      -- vim.api.nvim_create_autocmd({"FileType"}, {
      --     pattern = {"markdown", "mail", "text", "gitcommit"},
      --     callback = function()
      --         require'cmp'.setup.buffer {
      --             completion = { autocomplete = false }
      --         }
      --     end
      -- })

      -- I don't want to have the menu popping up all the time. Going to try with a delay and see if this makes it less annoying.
      -- https://github.com/hrsh7th/nvim-cmp/issues/715#issuecomment-1806944224
      -- local timer = nil
      -- local delay = 200
      -- vim.api.nvim_create_autocmd({ "TextChangedI", "CmdlineChanged" }, {
      --   pattern = "*",
      --   callback = function()
      --     if timer then
      --       vim.loop.timer_stop(timer)
      --       timer = nil
      --     end

      --     local function doesNotMatchFileTypes(targetString)
      --       local options = { "markdown", "text", "mail" } -- Your list of options
      --       for _, option in ipairs(options) do
      --         if targetString == option then
      --           return false -- Return false if the target matches any option
      --         end
      --       end
      --       return true -- Return true if no match was found after iterating through all options
      --     end

      --     timer = vim.loop.new_timer()
      --     timer:start(delay, 0, vim.schedule_wrap(function()
      --       if doesNotMatchFileTypes(vim.bo.filetype) then
      --         require('cmp').complete({ reason = require('cmp').ContextReason.Auto })
      --       end
      --     end))
      --   end
      -- })
    end
  }
}
