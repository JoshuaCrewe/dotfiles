-- Modes
--   normal_mode = "n"
--   insert_mode = "i"
--   visual_mode = "v"
--   visual_block_mode = "x"
--   term_mode = "t"
--   command_mode = "c"

map("", "<Space>", "<Nop>") -- map Space to a no-op
g.mapleader = ' '           -- space for leader
g.maplocalleader = ' '      -- space for local leader too

g.netrw_liststyle = 3

-- swtich between latest buffers with double space
map("n", "<leader><leader>", "<c-^>")

-- Copy to system clipboard
map("n", "<leader>y", "\"+yy")
map("v", "<leader>y", "\"+yy")

-- Inspect which tree shaker group to target
map('n', '<leader>tsh', ':TSHighlightCapturesUnderCursor<CR>')

-- Make j and k move by virtual lines instead of physical lines, but only when
-- not used in the count mode (e.g. 3j). This is great when 'wrap' and
-- 'relativenumber' are used.
-- Based on http://stackoverflow.com/a/21000307/2580955
cmd("noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')")
cmd("noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')")

-- Run pen.js in the background for markdown rendering
vim.api.nvim_create_user_command("Pen", function()
    cmd("te pen %")
    cmd("bprevious")
    print("Running on localhost:6060")
end, {})

-- Stay in visual mode when indenting
map('v', '<', '<gv')
map('v', '>', '>gv')

-- Replace netrw with a regular terminal
map('n', '_', '":e term://cd \'" .. expand("%:p:h") .. "/\' && " .. getenv("SHELL") .. "<cr>"',
    { noremap = true, expr = true })

-- Escape out of a terminal quicker
map('t', '<Esc><Esc>', '<c-\\><c-n>')


-- @TODO : Port this
vim.cmd([[
    " A vim implementation of Sublime Texts Multiple Cursors
    " http://www.kevinli.co/posts/2017-01-19-multiple-cursors-in-500-bytes-of-vimscript/
    let g:mc = "y/\\V\<C-r>=escape(@\", '/')\<CR>\<CR>"

    " Visually select, cn to change and dot to repeat.
    vnoremap <expr> cn g:mc . "``cgn"
    vnoremap <expr> cN g:mc . "``cgN"
]])

-- Cursor on word and use cn to change and the dot command to repeat.
map('n', 'cn', '*``cgn')
map('n', 'cN', '*``cgN')

-- Visually select, cn to change and dot to repeat.
-- map('v', 'cn', g:mc . "``cgn")
-- map('v', 'cN', g:mc . "``cgN")


-- @TODO: Port this
vim.cmd([[
    " Zero will go to the start of the content first and then the very start
    function! ToggleHomeZero()
    let pos = getpos('.')
    execute "normal! ^"
    if pos == getpos('.')
        execute "normal! 0"
    endif
    endfunction
    nnoremap 0 :call ToggleHomeZero()<CR>
]])


-- " A list of mappings to auto close brackets sometimes
-- map('i', '(<CR>', '(<CR>)<Esc>O')
-- map('i', '{<CR>', '{<CR>}<Esc>O')
-- map('i', '{;', '{<CR>};<Esc>O')
-- map('i', '{,', '{<CR>},<Esc>O')
-- map('i', '[<CR>', '[<CR>]<Esc>O')
-- map('i', '[;', '[<CR>];<Esc>O')
-- map('i', '[,', '[<CR>],<Esc>O')

map('n', '<leader>gb', ':Git blame<CR>')


vim.keymap.set("n", "Q", "<nop>")
vim.keymap.set("n", "q:", "<nop>")
vim.keymap.set("n", "Q:", "<nop>")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
