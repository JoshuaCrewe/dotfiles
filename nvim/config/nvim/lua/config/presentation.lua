-- require "usr.helpers"

-- Enter a minimal UI mode for slides
acmd({ "VimEnter" },
     { pattern = "*.slide",
       command = "set nocursorline nocursorcolumn norelativenumber nonumber noshowmode noshowcmd nohidden noruler laststatus=0"
     })

-- Move through open buffers with arrows
map('n', '<Left>', ':silent bp<CR> :redraw!<CR>')
map('n', '<Right>', ':silent bn<CR> :redraw!<CR>')
