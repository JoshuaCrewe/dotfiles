-- https://github.com/zanshin/dotfiles/tree/refactor/nvim
require 'usr.helpers'  -- Functions to wrap commands
require 'usr.options'  -- Anything that gets set
require 'usr.mappings' -- All the mappings
require 'usr.plugins'  -- All the plugins
require 'usr.autocmds' -- Auto commands
require 'usr.colors'   -- Theme
require 'usr.debug'    -- Debuggin tools

require 'config.presentation'
