# Neovim

I resisted for a long time. I felt like I was missing out on some modern things and that I was maybe hindering myself by not embracing modern things. Like completion. Or more advanced code parsing (treesitter).

The jury is still out on most of that but at least I can try it all now. I stupidly wrote my own theme which I regret a lot but now I have loss aversion so will be sticking with it for now.

I wanted to make the config easier to work with by using multiple files as well as making it an all Lua config. Another silly decision really but one that I have mostly stuck with.
