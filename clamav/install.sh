#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"

if hash clamscan 2>/dev/null; then
  src="$dirname/bin/virus-scan-home.sh"
  dst="/usr/local/bin/virus-scan-home.sh"
  _sudo_link "$src" "$dst"

  src="$dirname/bin/virus-scan-root.sh"
  dst="/usr/local/bin/virus-scan-root.sh"
  _sudo_link "$src" "$dst"

  src="$dirname/config/clamav-scan-home.service"
  dst="/etc/systemd/system/clamav-scan-home.service"
  chmod 644 "$src"
  _sudo_link "$src" "$dst"

  src="$dirname/config/clamav-scan-root.service"
  dst="/etc/systemd/system/clamav-scan-root.service"
  chmod 644 "$src"
  _sudo_link "$src" "$dst"

  src="$dirname/config/clamav-scan-home.timer"
  dst="/etc/systemd/system/clamav-scan-home.timer"
  chmod 644 "$src"
  _sudo_link "$src" "$dst"

  src="$dirname/config/clamav-scan-root.timer"
  dst="/etc/systemd/system/clamav-scan-root.timer"
  chmod 644 "$src"
  _sudo_link "$src" "$dst"

  _sudo_enable 'clamav-scan-home.timer'
  _sudo_enable 'clamav-scan-root.timer'
fi
