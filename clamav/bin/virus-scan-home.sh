#!/usr/bin/env bash

/usr/bin/clamscan -r -i  --exclude-dir="^/.cache/" --exclude-dir="^/sys/" --exclude-dir="^/dev/" --exclude-dir="^/proc/"  --exclude-dir="^/home/joshua/.local/share/virus/" --move=/home/joshua/.local/share/virus /home/joshua
