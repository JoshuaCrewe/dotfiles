# Scanning for viruses

There are two prongs to this fork. One is a more regular scan of the home directory (aiming for twice a day), while the other is a full scan of the root directory (something like once a week).

To manually start a scan run `systemctl --user start clamav-scan-home.service`
