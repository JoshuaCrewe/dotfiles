# Git

[https://git-scm.com/](https://git-scm.com/)

I have only ever used git. It is amazing software. There are very few aliases which I have come to rely on. So much so that it is frustrating typing out a whole git command when ssh'd into a server.

There is a local config which is separate from the main config, this is so you can define you identity on each machine individually. You might want your work life and your personal life separate. Copy the example local config to the config directory to have that work. It is also part of the install script `scripts/install` which will set this up if starting from scratch.

There are enough aliases and config that I forget about most of them, with most of the functionality covered by `tig` and `gitui`.
