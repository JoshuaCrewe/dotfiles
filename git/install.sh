#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"

src="$dirname/config/gitconfig"
dst="$HOME/.config/git/config"
_link "$src" "$dst"

src="$dirname/config/gitconfig.local"
dst="$HOME/.config/git/config.local"
_link "$src" "$dst"

src="$dirname/config/gitignore"
dst="$HOME/.config/git/gitignore"
_link "$src" "$dst"

src="$dirname/config/git-aliases"
dst="$HOME/.config/git/git-aliases"
_link "$src" "$dst"
