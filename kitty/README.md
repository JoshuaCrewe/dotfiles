# Kitty

I started running this terminal client when I was looking for slightly fancy things. Specifically Images in the terminal. When it works it is pretty useful, for example when using `nnn` as a File Browser. There is also a bit of communication between Kitty and Vim which allows me to use a terminal as the file browser in Vim. 

My preference would be to shirk this fancyness and go back to Alacritty. This would mean probably not using the terminal so much inside Vim and also revisiting the terminal file browser life. The latter needs looking at anyway I think as it isn't perfect.

Really I don't want to be using a Python tool. Which is silly as it works really well. The communication between Vim and Kitty basically touches too many things and I don't remember how it works. Using oil.nvim would be sensible and not needing images in a terminal would be a good shout.
