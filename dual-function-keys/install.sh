#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"


if [[ $is_linux == 1 ]]; then
    src="$dirname/config/dual-function-keys.yaml"
    dst="/etc/interception/dual-function-keys.yaml"
    _sudo_link "$src" "$dst"

    src="$dirname/config/udevmon.yaml"
    dst="/etc/interception/udevmon.yaml"
    _sudo_link "$src" "$dst"

    src="$dirname/config/udevmon.service"
    dst="/etc/systemd/system/udevmon.service"
    chmod 644 "$src"
    _sudo_link "$src" "$dst"
    _sudo_enable 'udevmon.service'
    _sudo_start 'udevmon.service'
fi

