# Dual Function Keys

[https://gitlab.com/interception/linux/plugins/dual-function-keys](https://gitlab.com/interception/linux/plugins/dual-function-keys)

Once upon a time I read [an article](https://stevelosh.com/blog/2012/10/a-modern-space-cadet/#s13-control-escape) by Steve Losh which described a keyboard focusses set up. Since then, multifunction keys and a Hyper key have been number one on the list of features to maintain for Linux on the Desktop.

The current iterating work on X and on Wayland which is groovy. Practically you end up with a YAML file to set your mappings. Things like mapping left and right shift to `(` and `)` has been fab. Using Tab as tab when tapped and Hyper when held has been amazing. Although a bit of a curse when it isn't there.
