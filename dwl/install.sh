#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(
  cd "$(dirname "$0")"
  pwd
)
source "$dirname/../scripts/_shared.sh"

if [[ ! -d $HOME/Data/code/dwl ]]; then
  if [[ ! -d $HOME/Data/code/ ]]; then
    mkdir -p $HOME/Data/code/
  fi
  git clone https://codeberg.org/joshuacrewe/dwl $HOME/Data/code/dwl
fi

src="$dirname/config/config.h"
dst="$HOME/Data/code/dwl/config.h"
_link "$src" "$dst"
