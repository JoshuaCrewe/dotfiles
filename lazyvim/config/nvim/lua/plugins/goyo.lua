return {
    {
        "junegunn/goyo.vim",
        ft = { "markdown", "mail" },
        config = function()
            vim.api.nvim_create_autocmd({ "Filetype" }, {
                pattern = { "markdown", "mail" },
                command = "Goyo 80x80% | set wrap linebreak breakindent spell",
            })
            -- vim.api.nvim_create_autocmd({ "VimEnter" }, {
            --     pattern = { "*.md", "/tmp/neomutt*" },
            --     command = "Goyo 80x80% | set wrap linebreak breakindent spell",
            -- })
            -- @TODO : Port this
            vim.cmd([[
                " The next two functions make Goyo behave in a more sane way
                function! GoyoBefore()
                let b:quitting = 0
                let b:quitting_bang = 0
                autocmd QuitPre <buffer> let b:quitting = 1
                cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
                endfunction

                function! GoyoAfter()
                " Quit Vim if this is the only remaining buffer
                if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
                    if b:quitting_bang
                    qa!
                    else
                    qa
                    endif
                endif
                endfunction

                let g:goyo_callbacks = [function('GoyoBefore'), function('GoyoAfter')]
            ]])
        end,
    },
}
