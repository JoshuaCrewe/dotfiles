return {
  "nvim-treesitter/nvim-treesitter",
  lazy = true,
  opts = {
    ensure_installed = {
      "bash",
      "javascript",
      "json",
      "lua",
      "markdown",
      "markdown_inline",
      "regex",
      "tsx",
      "typescript",
      "vim",
      "yaml",
      "vue",
    },
  },
}
