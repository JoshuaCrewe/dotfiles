return {
  {
    dir = "~/Data/code/seoul256.nvim",
    -- "https://codeberg.org/joshuacrewe/seoul256.nvim",
    dependencies = {
      "rktjmp/lush.nvim"
    },

  },

  -- Configure LazyVim to load gruvbox
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "seoul256",
    },
  },
}
