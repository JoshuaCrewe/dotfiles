return {
    { "akinsho/bufferline.nvim", enabled = false },
    { "nvim-lualine/lualine.nvim", enabled = false },
    { "folke/noice.nvim", enabled = false },
    { "rcarriga/nvim-notify", enabled = false },
    { "echasnovski/mini.ai", enabled = false },
}
