-- Uncomment this gate to enable nvim for machines with very few resources
-- stylua: ignore
if true then return {} end

return {
    { "", enabled = false },
    { "hrsh7th/cmp-nvim-lsp", enabled = false },
    { "neovim/nvim-lspconfig", enabled = false },
    { "williamboman/mason.nvim", enabled = false },
    { "williamboman/mason-lspconfig.nvim", enabled = false },
    {
        "nvim-treesitter/nvim-treesitter",
        opts = {
            ensure_installed = {},
        },
    },
}
