return {
  "https://codeberg.org/JoshuaCrewe/telescope-notmuch.nvim.git",
  dependencies = {
    'nvim-telescope/telescope.nvim',
  },
  ft = { 'mail' },
  keys = {
    -- Quickly pop an email address in
    { "<c-x><c-p>", "<cmd>Telescope notmuch theme=cursor<cr>", desc = "Find Email" },
  },
  config = function()
    require('telescope').load_extension("notmuch")
  end,
}
