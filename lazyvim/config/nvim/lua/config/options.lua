-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.opt.laststatus = 2 -- global statusline
vim.opt.clipboard = "" -- I want a different experience internall and clipboard i.e. no clobbering
