-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

local map = LazyVim.safe_keymap_set

-- Copy to system clipboard
map("n", "<leader>y", '"+yy')
map("v", "<leader>y", '"+yy')

-- Inspect which tree shaker group to target
map("n", "<leader>tsh", ":TSHighlightCapturesUnderCursor<CR>")

-- Run pen.js in the background for markdown rendering
vim.api.nvim_create_user_command("Pen", function()
  vim.cmd("te pen %")
  vim.cmd("bprevious")
  print("Running on localhost:6060")
end, {})

-- Stay in visual mode when indenting
map("v", "<", "<gv")
map("v", ">", ">gv")

-- Escape out of a terminal quicker
map("t", "<Esc><Esc>", "<c-\\><c-n>")

-- @TODO : Port this
vim.cmd([[
    " A vim implementation of Sublime Texts Multiple Cursors
    " http://www.kevinli.co/posts/2017-01-19-multiple-cursors-in-500-bytes-of-vimscript/
    let g:mc = "y/\\V\<C-r>=escape(@\", '/')\<CR>\<CR>"

    " Visually select, cn to change and dot to repeat.
    vnoremap <expr> cn g:mc . "``cgn"
    vnoremap <expr> cN g:mc . "``cgN"
]])

-- Cursor on word and use cn to change and the dot command to repeat.
map("n", "cn", "*``cgn")
map("n", "cN", "*``cgN")

map("n", "<leader>gb", ":Git blame<CR>")

vim.keymap.set("n", "Q", "<nop>")
vim.keymap.set("n", "q:", "<nop>")
vim.keymap.set("n", "Q:", "<nop>")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

map("n", "]<space>", "o<esc>k")
map("n", "[<space>", "O<esc>j")

vim.keymap.del({ "n", "i" }, "<M-j>")
vim.keymap.del({ "n", "i" }, "<M-k>")
vim.keymap.del({ "n" }, "H")
vim.keymap.del({ "n" }, "L")

map("n", "z=", ":Telescope spell_suggest<CR>")
