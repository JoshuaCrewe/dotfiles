# Grease Monkey

[https://www.greasespot.net/](https://www.greasespot.net/)

This is a collection of scripts which can be automatically injected into web pages via the Grease Monkey browser extension. 

Install the extension and then copy and paste each script here into a user script on the extensions settings page.
