#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"

for i in $dirname/config/user-scripts/*.js; do
    src="$i"
    dst="$HOME/.config/qutebrowser/greasemonkey/$(basename ${i})"
    _link "$src" "$dst"
done
