// ==UserScript==
// @name     Email Reader
// @version  1
// @grant    none
// ==/UserScript==

if (window.location.href.indexOf(".cache/mutt-wizard/files/") > -1) {
  var styles = `
    body.contained {
        width: 100%;
        margin: 0 auto !important;
        margin-top: 5rem !important;
    }

    body:not(.contained) {
        max-width: 800px;
        margin: 0 auto !important;
        padding: 4rem 1rem;
        font-size: 18px;
    }

    img:not(.icon) {
        max-width: 100%;
        height: auto !important;
    }

    img[src^="https://www.venncreative.co.uk/this-gif.gif"] {
        height: unset !important;
        margin-inline: 0% !important;
        width: unset !important;
    }
    img[src^="https://venncreative.co.uk/venn-signature/"],
    img[src^="http://venncreative.co.uk/venn-signature/"] {
        height: unset !important;
        width: 12px !important;
        height: 12px !important;
    }  
    `;

  var styleSheet = document.createElement("style");
  styleSheet.innerText = styles;
  document.head.appendChild(styleSheet);
}
