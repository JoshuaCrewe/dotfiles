// ==UserScript==
// @name     Unnamed Script 122968
// @version  1
// @grant    none
// ==/UserScript==

if (window.location.href.indexOf("tickets.cloudways.com") > -1) {
  var styles = `
    body {
      font-weight: normal;
      color: black;
    }
  `;

  var styleSheet = document.createElement("style");
  styleSheet.innerText = styles;
  document.head.appendChild(styleSheet);
}
