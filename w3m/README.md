# w3m

Finally have a use case for using a terminal based browser and was pleasantly surprised to see how much great stuff there was on the Arch Wiki about it. A sort of Vim mode and hints for links was great. It might even be able to do images which sounds pretty neat.

I have a really low powered computer which has about 2GB of RAM. It is a test bed for software and might end up being a writing machine. It doesn't like Firefox being open. W3M might be the minimal cripped web experience I need on this machine.

## Resources

[bible.oremus.org](bible.oremus.org) is a website which will search bible verses from w3m without too much weirdness

The work flow which I have settled on for now is to make use of duckduckgo.com (the lite version) with `site:biblehub.com <query>` to find the relevant information
