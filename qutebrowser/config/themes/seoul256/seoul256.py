C = {
    "Background" : {
        "default"  : "#121212",
        "emphasis" : "#292929",
        "muted"    : "#1c1c1c",
        "status"   : "#875f5f",
    },
    "Foreground" : {
        "default"  : "#d9d9d9",
        "emphasis" : "#ffffff",
        "muted"    : "#3a3a3a",
        "status"   : "#d7d7af",
    },
    "Special" : {
        "comment"  : "#719872",
        "visual"   : "#005f5f",
        "visualEm" : "#004747",
        "warning"  : "#d7005f",
        "search"   : "#005f87",
        "menu"     : "#ffd7d7",
        "menuSel"  : "#87005f",
    },
    "Base" : {
        "black0"     : "#4e4e4e",
        "black1"     : "#626262",

        "white0"     : "#d0d0d0",
        "white1"     : "#e4e4e4",

        "red0"       : "#d68787",
        "red1"       : "#d75f87",

        "blue0"      : "#85add4",
        "blue1"      : "#add4fb",

        "green0"     : "#5f865f",
        "green1"     : "#87af87",

        "palegreen0" : "#DFDEBD",
        "palegreen1" : "#b3b061",

        "yellow0"    : "#d8af5f",
        "yellow1"    : "#ffd787",

        "magenta0"   : "#d7afaf",
        "magenta1"   : "#ffafaf",

        "cyan0"      : "#87afaf",
        "cyan1"      : "#87d7d7",

        "orange0"    : "#E19972",
        "orange1"    : "#d7875f",

        "brown0"     : "#af875f",
        "brown1"     : "#876543",

        "purple0"    : "#8787af",
        "purple1"    : "#8787ff",
    }
}


c.colors.statusbar.normal.bg = C["Background"]["status"]
c.colors.statusbar.normal.fg = C["Foreground"]["status"]
c.colors.statusbar.url.fg = C["Foreground"]["status"]
c.colors.statusbar.url.success.http.fg = C["Foreground"]["status"]
c.colors.statusbar.url.success.https.fg = C["Foreground"]["status"]

c.colors.tabs.bar.bg = C["Background"]["default"]
# c.colors.tabs.even.bg = "#1c1c1c"
# c.colors.tabs.odd.bg = "#292929"

# c.colors.tabs.even.fg = "#d9d9d9"
# c.colors.tabs.odd.fg = "#d9d9d9"

# c.colors.tabs.pinned.odd.bg = c.colors.tabs.odd.bg
# c.colors.tabs.pinned.even.bg = c.colors.tabs.even.bg

# c.colors.tabs.selected.even.bg = "#005f5f"
# c.colors.tabs.selected.odd.bg = "#005f5f"
# c.colors.tabs.pinned.selected.even.bg = "#005f5f"
# c.colors.tabs.pinned.selected.odd.bg = "#005f5f"


# c.colors.statusbar.normal.bg = "#875f5f"
# c.colors.statusbar.normal.fg = "#d7d7af"
# c.colors.statusbar.url.fg = "#d7d7af"
# c.colors.statusbar.url.success.http.fg = "#d7d7af"
# c.colors.statusbar.url.success.https.fg = "#d7d7af"
