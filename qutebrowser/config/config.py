# Resources :
# - https://github.com/gicrisf/qute-config/blob/main/config.org

config.load_autoconfig(False)

config.bind('j', 'cmd-run-with-count 3 scroll down')
config.bind('k', 'cmd-run-with-count 3 scroll up')
config.bind('<Ctrl-j>', 'scroll down')
config.bind('<Ctrl-k>', 'scroll up')
config.bind('<Space>t', "config-cycle tabs.show always never")
config.bind('<Space><Space>', "tab-focus last")
config.bind('<Space>rr', "config-source")
config.bind('<Space>i', "devtools")

c.tabs.position = "left"
c.tabs.show = "never"
c.tabs.width = "15%"
c.tabs.max_width = 15

c.tabs.pinned.shrink = False
c.scrolling.smooth = True
c.scrolling.bar = 'when-searching'

# c.hints.mode = 'number'
c.hints.mode = 'letter'

config.source('themes/qute-city-lights/city-lights-theme.py')
config.source('themes/seoul256/seoul256.py')
