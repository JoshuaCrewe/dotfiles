#!/usr/bin/env bash

set -u # error out if variable is not set
set -e # exit if returns non true

dirname=$(cd "$(dirname "$0")"; pwd)
source "$dirname/../scripts/_shared.sh"

src="$dirname/config/config.py"
dst="$HOME/.config/qutebrowser/config.py"
_link "$src" "$dst"

src="$dirname/config/themes"
dst="$HOME/.config/qutebrowser/themes"
_link "$src" "$dst"
